TinyProxy-Splitter
##################

A proxy setup for routing of http(s) traffic to one of multiple upstream socks and/or http proxies, based on the destination address. This setup is particularly useful when used in combination with one or more ssh tunnel socks proxies; Just set your http(s)_proxy environment variable and http(s) traffic will be routed via the socks proxy. Nifty.

`Tinyproxy <https://tinyproxy.github.io>`_ is used to route the traffic (`Source <https://github.com/tinyproxy/tinyproxy>`_).

Usage
=====

* Copy tinyproxy.conf.example to tinyproxy.conf and configure it to your needs.
* Add the following to your .bashrc file::

    p() {
        /path/to/tinyproxy-splitter/run.sh $@
        export http_proxy=http://localhost:8080
        export https_proxy=http://localhost:8080
        export HTTP_PROXY=http://localhost:8080
        export HTTPS_PROXY=http://localhost:8080
    }

    alias np="unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY"

* Source .bashrc or restart your terminal sessions.
* Exectute the command 'np' to disable use of the proxy and 'p' to conditionaly build and then reactivate it. Add the '-r' parameter to p to force rebuilding of the image.

