#!/bin/bash

# This script is can be adjusted and used for diagnostic purposes.
# It is not in any way needed for the operation of te proxy.

PROXY=localhost:8008

test_proxy() {
    URL=$1
    if (curl -vx $PROXY $URL &>/dev/null) ; then
        echo "SUCCES: $URL"
    else
        echo "FAILED: $URL"
    fi
}

echo "Testing connections over proxy $PROXY"
# an external address that should be routed via your local machine
test_proxy http://ifconfig.co

# a corporate address that must be routed via the http_proxy
test_proxy http://site.corporation.com

# a corparate address that should be tunneled via a socks proxy
test_proxy http://some.internal.address.local

