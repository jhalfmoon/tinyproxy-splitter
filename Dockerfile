#=================================================
# https://hub.docker.com/_/alpine/tags
# https://github.com/tinyproxy/tinyproxy

#=================================================
# stage 1 - Fetch / Build binaries
FROM alpine:3.18 as fetacher
ENV TINYPROXY_VERSION="1.11.1"
RUN set -ex \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && apk update \
    && apk upgrade \
    && apk add --no-cache bash curl wget gzip bzip2 zstd build-base
RUN cd /usr/local \
    && curl -L https://github.com/tinyproxy/tinyproxy/releases/download/${TINYPROXY_VERSION}/tinyproxy-${TINYPROXY_VERSION}.tar.gz | tar xz \
    && cd tinyproxy-${TINYPROXY_VERSION} \
    && ./configure --prefix=''\
    && make -j4 \
    && mv src/tinyproxy docs/man8/*.8 docs/man5/*.5 /

#=================================================
# stage 2 - Build clean container image
FROM alpine:3.18
RUN set -ex \
    && apk update \
    && apk upgrade \
    && apk add --no-cache bash curl netcat-openbsd
RUN mkdir -p /etc/tinyproxy
COPY tinyproxy.conf.example /etc/tinyproxy
COPY --from=0 /tinyproxy        /usr/local/bin
COPY --from=0 /tinyproxy.8      /usr/share/man/man8
COPY --from=0 /tinyproxy.conf.5 /usr/share/man/man5
ENTRYPOINT [ "/usr/local/bin/tinyproxy" , "-d" ]
CMD ["/bin/bash", "-c", "while true; do ping localhost; sleep 60;done"]


