#!/bin/bash

# Build and run this appliaction
# Used -r to terminate any running instance and rebuild the image

# Note: Name is for both container image and container instance
NAME=tinyproxy-splitter
REBUILD=0
PORT=8008

HERE=$(dirname $(readlink -f $0))
cd $HERE

while getopts "r" opt; do
  case $opt in
    r)  REBUILD=1
        ;;
  esac
done

if [[ $REBUILD -gt 0 ]] ; then
    echo "Removing any existing instance of $NAME..."
    docker rm -f $NAME 2> /dev/null
    docker rmi -f $NAME 2> /dev/null
fi

if [[ ! -e tinyproxy.conf ]] ; then
    echo "WARNING: File 'tinyproxy.conf' not found."
    echo "Using tinyproxy.conf.example as a base."
    cp tinyproxy.conf.example tinyproxy.conf
fi

# Alternative check to check is tinyproxy is running
#if ! ( nc -z 127.0.0.1 8008 ); then

EXIST=$(docker container list -a --format "{{lower .Names}}")
RUNNING=$(docker ps --format "{{lower .Names}}")
if [[ "$EXIST" =~ $NAME ]] ; then
    if [[ "$RUNNING" =~ $NAME ]] ; then
        echo "Container $NAME is already running."
    else
        echo "Container $NAME already exists. Starting it..."
        docker start $NAME
        timeout 1 docker logs -f $NAME | tail -n 30
    fi
else
    echo "BUilding container image..."
    unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY
    if ! (docker buildx &> /dev/null) ; then
        BUILDX=''
        echo
        echo "NOTE: buildx docker plugin not found. You may want to install it."
        echo
        sleep 3
    else
        BUILDX='buildx'
    fi
    docker $BUILDX build -f Dockerfile -t $NAME .

    echo "Starting container..."
    docker run -d --name $NAME -p $PORT:8080 --mount type=bind,source="$(pwd)"/tinyproxy.conf,target=/etc/tinyproxy/tinyproxy.conf $NAME

    echo "Tailing tinyproxy logs..."
    timeout 1 docker logs -f $NAME
    echo
    echo "To use this proxy, run the following:"
    echo "export http_proxy=http://localhost:$PORT"
    echo "export https_proxy=http://localhost:$PORT"
    echo "export HTTP_PROXY=http://localhost:$PORT"
    echo "export HTTPS_PROXY=http://localhost:$PORT"
fi

